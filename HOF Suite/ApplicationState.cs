﻿using Hofsuite.Core;
using Hofsuite.Core.Structs;

using System;

namespace Hofsuite
{
    static class ApplicationState
    {
        static ShownTab _tab;
        static string _filename;
        static bool _unsaved;
        static Hof _hof;
        static Item _item;

        public static event EventHandler TabChanged;
        public static event EventHandler FilenameChanged;
        public static event EventHandler UnsavedChanged;
        public static event EventHandler HofChanged;
        public static event EventHandler ItemChanged;

        public static event EventHandler MaskChanged;
        public static event EventHandler ItemPropertiesChanged;

        public static ShownTab Tab
        {
            get => _tab;
            set
            {
                _tab = value;
                TabChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        public static string Filename
        {
            get => _filename;
            set
            {
                _filename = value;
                FilenameChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        public static bool IsUnsaved
        {
            get => _unsaved;
            set
            {
                _unsaved = value;
                UnsavedChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        public static Hof Hof
        {
            get => _hof;
            set
            {
                _hof = value;
                HofChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        public static Item Item
        {
            get => _item;
            set
            {
                _item = value;
                ItemChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        public static void InvokeMaskChangedEvent()
        {
            MaskChanged?.Invoke(null, EventArgs.Empty);
        }

        public static void InvokeItemPropertiesChangedEvent()
        {
            ItemPropertiesChanged?.Invoke(null, EventArgs.Empty);
        }

        public static void ResetItemChangedSubscriptions()
        {

        }
    }
}
