﻿namespace Hofsuite.GUI.Controls
{
    partial class RouteView
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RouteView));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxStops = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.moveStopUp = new System.Windows.Forms.Button();
            this.moveStopDown = new System.Windows.Forms.Button();
            this.removeStop = new System.Windows.Forms.Button();
            this.addStop = new System.Windows.Forms.Button();
            this.reverseStops = new System.Windows.Forms.Button();
            this.allStops = new System.Windows.Forms.ListBox();
            this.routeStops = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.textDestination = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textTour = new System.Windows.Forms.MaskedTextBox();
            this.textLine = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.groupBoxStops.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.SystemColors.Desktop;
            resources.ApplyResources(this.splitContainer, "splitContainer");
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.BackColor = System.Drawing.SystemColors.Control;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer.Panel2.Controls.Add(this.tableLayoutPanel);
            this.splitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitterMoved);
            // 
            // tableLayoutPanel
            // 
            resources.ApplyResources(this.tableLayoutPanel, "tableLayoutPanel");
            this.tableLayoutPanel.Controls.Add(this.groupBoxStops, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.groupBoxSettings, 0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            // 
            // groupBoxStops
            // 
            this.groupBoxStops.Controls.Add(this.tableLayoutPanel1);
            resources.ApplyResources(this.groupBoxStops, "groupBoxStops");
            this.groupBoxStops.Name = "groupBoxStops";
            this.groupBoxStops.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.moveStopUp, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.moveStopDown, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.removeStop, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.addStop, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.reverseStops, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.allStops, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.routeStops, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // moveStopUp
            // 
            resources.ApplyResources(this.moveStopUp, "moveStopUp");
            this.moveStopUp.Image = global::Hofsuite.Properties.Resources.Up;
            this.moveStopUp.Name = "moveStopUp";
            this.toolTip.SetToolTip(this.moveStopUp, resources.GetString("moveStopUp.ToolTip"));
            this.moveStopUp.UseVisualStyleBackColor = true;
            this.moveStopUp.Click += new System.EventHandler(this.MoveStopUp);
            // 
            // moveStopDown
            // 
            resources.ApplyResources(this.moveStopDown, "moveStopDown");
            this.moveStopDown.Image = global::Hofsuite.Properties.Resources.Down;
            this.moveStopDown.Name = "moveStopDown";
            this.toolTip.SetToolTip(this.moveStopDown, resources.GetString("moveStopDown.ToolTip"));
            this.moveStopDown.UseVisualStyleBackColor = true;
            this.moveStopDown.Click += new System.EventHandler(this.MoveStopDown);
            // 
            // removeStop
            // 
            resources.ApplyResources(this.removeStop, "removeStop");
            this.removeStop.Image = global::Hofsuite.Properties.Resources.Left;
            this.removeStop.Name = "removeStop";
            this.toolTip.SetToolTip(this.removeStop, resources.GetString("removeStop.ToolTip"));
            this.removeStop.UseVisualStyleBackColor = true;
            this.removeStop.Click += new System.EventHandler(this.RemoveStop);
            // 
            // addStop
            // 
            resources.ApplyResources(this.addStop, "addStop");
            this.addStop.Image = global::Hofsuite.Properties.Resources.Right;
            this.addStop.Name = "addStop";
            this.toolTip.SetToolTip(this.addStop, resources.GetString("addStop.ToolTip"));
            this.addStop.UseVisualStyleBackColor = true;
            this.addStop.Click += new System.EventHandler(this.AddStop);
            // 
            // reverseStops
            // 
            resources.ApplyResources(this.reverseStops, "reverseStops");
            this.reverseStops.Image = global::Hofsuite.Properties.Resources.Change;
            this.reverseStops.Name = "reverseStops";
            this.toolTip.SetToolTip(this.reverseStops, resources.GetString("reverseStops.ToolTip"));
            this.reverseStops.UseVisualStyleBackColor = true;
            this.reverseStops.Click += new System.EventHandler(this.ReverseStop);
            // 
            // allStops
            // 
            resources.ApplyResources(this.allStops, "allStops");
            this.allStops.FormattingEnabled = true;
            this.allStops.Name = "allStops";
            this.tableLayoutPanel1.SetRowSpan(this.allStops, 7);
            // 
            // routeStops
            // 
            resources.ApplyResources(this.routeStops, "routeStops");
            this.routeStops.FormattingEnabled = true;
            this.routeStops.Name = "routeStops";
            this.tableLayoutPanel1.SetRowSpan(this.routeStops, 7);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.textDestination);
            this.groupBoxSettings.Controls.Add(this.label3);
            this.groupBoxSettings.Controls.Add(this.label6);
            this.groupBoxSettings.Controls.Add(this.label2);
            this.groupBoxSettings.Controls.Add(this.textTour);
            this.groupBoxSettings.Controls.Add(this.textLine);
            this.groupBoxSettings.Controls.Add(this.label1);
            this.groupBoxSettings.Controls.Add(this.textName);
            resources.ApplyResources(this.groupBoxSettings, "groupBoxSettings");
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.TabStop = false;
            // 
            // textDestination
            // 
            this.textDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.textDestination.FormattingEnabled = true;
            resources.ApplyResources(this.textDestination, "textDestination");
            this.textDestination.Name = "textDestination";
            this.textDestination.SelectedIndexChanged += new System.EventHandler(this.DestinationChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            this.toolTip.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.toolTip.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // textTour
            // 
            resources.ApplyResources(this.textTour, "textTour");
            this.textTour.Name = "textTour";
            this.textTour.TextChanged += new System.EventHandler(this.TourChanged);
            // 
            // textLine
            // 
            resources.ApplyResources(this.textLine, "textLine");
            this.textLine.Name = "textLine";
            this.textLine.TextChanged += new System.EventHandler(this.LineChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.toolTip.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // textName
            // 
            resources.ApplyResources(this.textName, "textName");
            this.textName.Name = "textName";
            this.textName.TextChanged += new System.EventHandler(this.NameChanged);
            // 
            // RouteView
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Name = "RouteView";
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.groupBoxStops.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.GroupBox groupBoxStops;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox textLine;
        private System.Windows.Forms.ComboBox textDestination;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button moveStopUp;
        private System.Windows.Forms.Button moveStopDown;
        private System.Windows.Forms.Button removeStop;
        private System.Windows.Forms.Button addStop;
        private System.Windows.Forms.Button reverseStops;
        private System.Windows.Forms.ListBox allStops;
        private System.Windows.Forms.ListBox routeStops;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox textTour;
    }
}
