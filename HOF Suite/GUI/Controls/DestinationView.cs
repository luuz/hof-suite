﻿using Hofsuite.Core;
using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;

using System;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    public partial class DestinationView : UserControl
    {
        public DestinationView()
        {
            Initializing = true;

            InitializeComponent();
            splitContainer.SplitterDistance = SettingsBusiness.SplitterDistance;

            Control newControl = new ItemListControl(ItemType.Destination)
            {
                Dock = DockStyle.Fill
            };
            splitContainer.Panel1.Controls.Add(newControl);

            newControl = new StringListView(StringType.Destination)
            {
                Dock = DockStyle.Fill
            };
            groupBoxStrings.Controls.Add(newControl);
            Initializing = false;

            LoadMask(this, EventArgs.Empty);
            LoadItem(this, EventArgs.Empty);

            ApplicationState.MaskChanged += LoadMask;
            ApplicationState.ItemChanged += LoadItem;
            ApplicationState.TabChanged += TabChanged;
        }

        private void TabChanged(object sender, EventArgs e)
        {
            ApplicationState.MaskChanged -= LoadMask;
            ApplicationState.ItemChanged -= LoadItem;
            ApplicationState.TabChanged -= TabChanged;
        }

        private void LoadItem(object sender, EventArgs e)
        {
            if (ApplicationState.Item == null)
            {
                groupBoxSettings.Visible = false;
                groupBoxStrings.Visible = false;
            }
            else
            {
                groupBoxSettings.Visible = true;
                groupBoxStrings.Visible = true;

                Initializing = true;

                if (SettingsBusiness.MaskEnabled)
                {
                    int codeLength = ((Destination)ApplicationState.Item).Code.ToString().Length;
                    if (codeLength < 4) textCode.Mask = "000";
                    else if (codeLength == 4) textCode.Mask = "0000";
                    else if (codeLength == 5) textCode.Mask = "00000";
                    else textCode.Mask = "";
                }
                else
                {
                    textCode.Mask = "";
                }

                textName.Text = ApplicationState.Item.Name;
                checkAllexit.Checked = ((Destination)ApplicationState.Item).Allexit;
                textCode.Text = ((Destination)ApplicationState.Item).Code.ToString();

                Initializing = false;
            }
        }

        private void LoadMask(object sender, EventArgs e)
        {
            Initializing = true;
            checkMask.Checked = SettingsBusiness.MaskEnabled;

            if (SettingsBusiness.MaskEnabled && ApplicationState.Item != null)
            {
                int codeLength = ((Destination)ApplicationState.Item).Code.ToString().Length;
                if (codeLength < 4) textCode.Mask = "000";
                else if (codeLength == 4) textCode.Mask = "0000";
                else if (codeLength == 5) textCode.Mask = "00000";
                else textCode.Mask = "";
            }
            else
            {
                textCode.Mask = "";
            }

            Initializing = false;
        }

        bool Initializing { get; set; }

        private void SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (Initializing) return;
            SettingsBusiness.SplitterDistance = splitContainer.SplitterDistance;
        }

        private void CheckMaskChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            SettingsBusiness.MaskEnabled = checkMask.Checked;
        }

        private void NameChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            ApplicationState.Item.Name = textName.Text;
            ApplicationState.IsUnsaved = true;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }

        private void AllexitChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            ((Destination)ApplicationState.Item).Allexit = checkAllexit.Checked;
            ApplicationState.IsUnsaved = true;
        }

        private void CodeChanged(object sender, EventArgs e)
        {
            if (Initializing) return;

            if (int.TryParse(textCode.Text.Trim(), out int i)) {
                ((Destination)ApplicationState.Item).Code = i;
            }
            else
            {
                ((Destination)ApplicationState.Item).Code = 0;
            }
            ApplicationState.IsUnsaved = true;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }
    }
}
