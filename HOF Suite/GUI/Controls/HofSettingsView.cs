﻿using Hofsuite.Core;
using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;
using Hofsuite.Properties;

using System;
using System.IO;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    partial class HofSettingsView : UserControl
    {
        public HofSettingsView()
        {
            InitializeComponent();

            LoadApplicationState(this, EventArgs.Empty);

            ApplicationState.HofChanged += LoadApplicationState;
            ApplicationState.TabChanged += TabChanged;
        }

        private void TabChanged(object sender, EventArgs e)
        {
            ApplicationState.HofChanged -= LoadApplicationState;
            ApplicationState.TabChanged -= TabChanged;
        }

        Hof Hof => ApplicationState.Hof;

        void SetUnsaved() => ApplicationState.IsUnsaved = true;

        bool Initializing { get; set; }

        private void LoadApplicationState(object sender, EventArgs e)
        {
            Initializing = true;

            general_displayname.Text = Hof.Settings.Name;

            general_servicetrip.Items.Clear();
            foreach (var x in Hof.Destinations) general_servicetrip.Items.Add(x);
            if (Hof.Settings.Servicetrip != null && general_servicetrip.Items.Contains(Hof.Settings.Servicetrip))
                general_servicetrip.SelectedItem = Hof.Settings.Servicetrip;

            folder_announcements.Text = Hof.Settings.GetGlobalString(GlobalStringType.AnnouncementFolder);
            folder_rollsigns.Text = Hof.Settings.GetGlobalString(GlobalStringType.RollsignFolder);
            folder_sideplates.Text = Hof.Settings.GetGlobalString(GlobalStringType.SideplateFolder);

            ibiscode_5xx.Text = Hof.Settings.GetGlobalString(GlobalStringType.IBISCode5);
            ibiscode_8xx.Text = Hof.Settings.GetGlobalString(GlobalStringType.IBISCode8);
            ibiscode_9xx.Text = Hof.Settings.GetGlobalString(GlobalStringType.IBISCode9);
            
            try
            {
                exportDirs.Items.Clear();
                foreach (var x in PathBusiness.GetAllVehicleDirectories()) exportDirs.Items.Add(PathBusiness.GetDirectoryName(x));
            }
            catch (Exception)
            {
                
            }

            foreach (var x in Hof.Settings.VehicleDirectories)
            {
                if (!exportDirs.Items.Contains(x)) exportDirs.Items.Add(PathBusiness.GetDirectoryName(x));
                exportDirs.SetItemChecked(exportDirs.Items.IndexOf(PathBusiness.GetDirectoryName(x)), true);
            }

            Initializing = false;
        }

        private void DisplaynameChanged(object sender, EventArgs e)
        {
            if (Initializing) return;

            Hof.Settings.Name = general_displayname.Text;
            SetUnsaved();
        }

        private void ServicetripChanged(object sender, EventArgs e)
        {
            if (Initializing) return;

            Hof.Settings.Servicetrip = (Destination)general_servicetrip.SelectedItem;
            SetUnsaved();
        }

        private void AnnouncementsChanged(object sender, EventArgs e)
        {
            if (folder_announcements.Text.IsEmpty())
            {
                folder_announcements_create.Enabled = false;
                folder_announcements_exists.Image = null;
            }
            else if (Directory.Exists(PathBusiness.GetAnnouncementDirectory(folder_announcements.Text)))
            {
                folder_announcements_create.Enabled = false;
                folder_announcements_exists.Image = Resources.Yes;
            }
            else
            {
                folder_announcements_create.Enabled = true;
                folder_announcements_exists.Image = Resources.No;
            }
            if (Initializing) return;

            Hof.Settings.SetGlobalString(GlobalStringType.AnnouncementFolder, folder_announcements.Text);
            SetUnsaved();
        }

        private void RollsignChanged(object sender, EventArgs e)
        {
            if (folder_rollsigns.Text.IsEmpty())
            {
                folder_rollsigns_create.Enabled = false;
                folder_rollsigns_exists.Image = null;
            }
            else if (Directory.Exists(PathBusiness.GetRollsignDirectory(folder_rollsigns.Text)))
            {
                folder_rollsigns_create.Enabled = false;
                folder_rollsigns_exists.Image = Resources.Yes;
            }
            else
            {
                folder_rollsigns_create.Enabled = true;
                folder_rollsigns_exists.Image = Resources.No;
            }
            if (Initializing) return;

            Hof.Settings.SetGlobalString(GlobalStringType.RollsignFolder, folder_rollsigns.Text);
            SetUnsaved();
        }

        private void SideplateChanged(object sender, EventArgs e)
        {
            if (folder_sideplates.Text.IsEmpty())
            {
                folder_sideplates_create.Enabled = false;
                folder_sideplates_exists.Image = null;
            }
            else if (Directory.Exists(PathBusiness.GetSideplateDirectory(folder_sideplates.Text)))
            {
                folder_sideplates_create.Enabled = false;
                folder_sideplates_exists.Image = Resources.Yes;
            }
            else
            {
                folder_sideplates_create.Enabled = true;
                folder_sideplates_exists.Image = Resources.No;
            }
            if (Initializing) return;

            Hof.Settings.SetGlobalString(GlobalStringType.SideplateFolder, folder_sideplates.Text);
            SetUnsaved();
        }

        private void AnnouncementsClick(object sender, EventArgs e)
        {
            PathBusiness.CreateDirectory(PathBusiness.GetAnnouncementDirectory(folder_announcements.Text));
            Initializing = true;
            AnnouncementsChanged(sender, e);
            Initializing = false;
        }

        private void RollsignClick(object sender, EventArgs e)
        {
            PathBusiness.CreateDirectory(PathBusiness.GetRollsignDirectory(folder_rollsigns.Text));
            Initializing = true;
            RollsignChanged(sender, e);
            Initializing = false;
        }

        private void SideplateClick(object sender, EventArgs e)
        {
            PathBusiness.CreateDirectory(PathBusiness.GetSideplateDirectory(folder_sideplates.Text));
            Initializing = true;
            SideplateChanged(sender, e);
            Initializing = false;
        }

        private void Ibiscode9Changed(object sender, EventArgs e)
        {
            if (Initializing) return;

            Hof.Settings.SetGlobalString(GlobalStringType.IBISCode9, ibiscode_9xx.Text.Trim());
            SetUnsaved();
        }

        private void Ibiscode8Changed(object sender, EventArgs e)
        {
            if (Initializing) return;

            Hof.Settings.SetGlobalString(GlobalStringType.IBISCode8, ibiscode_8xx.Text.Trim());
            SetUnsaved();
        }

        private void Ibiscode5Changed(object sender, EventArgs e)
        {
            if (Initializing) return;

            Hof.Settings.SetGlobalString(GlobalStringType.IBISCode5, ibiscode_5xx.Text.Trim());
            SetUnsaved();
        }

        private void ExportSelectionChanged(object sender, EventArgs e)
        {
            if (Initializing) return;

            Hof.Settings.VehicleDirectories.Clear();
            foreach (var x in exportDirs.CheckedItems) Hof.Settings.VehicleDirectories.Add(x.ToString());
            SetUnsaved();
        }
    }
}
