﻿using Hofsuite.Core;
using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace Hofsuite.GUI.Controls
{
    public partial class RouteView : UserControl
    {
        public RouteView()
        {
            Initializing = true;

            InitializeComponent();
            splitContainer.SplitterDistance = SettingsBusiness.SplitterDistance;

            Control newControl = new ItemListControl(ItemType.Route)
            {
                Dock = DockStyle.Fill
            };
            splitContainer.Panel1.Controls.Add(newControl);
            Initializing = false;
            
            LoadItem(this, EventArgs.Empty);
            
            ApplicationState.ItemChanged += LoadItem;
            ApplicationState.TabChanged += TabChanged;
        }

        List<Destination> Destinations => ApplicationState.Hof.Destinations;
        List<Stop> Stops => ApplicationState.Hof.Stops;
        Route Route => (Route)ApplicationState.Item;

        private void TabChanged(object sender, EventArgs e)
        {
            ApplicationState.ItemChanged -= LoadItem;
            ApplicationState.TabChanged -= TabChanged;
        }

        private void LoadItem(object sender, EventArgs e)
        {
            if (ApplicationState.Item == null)
            {
                groupBoxSettings.Visible = false;
                groupBoxStops.Visible = false;
            }
            else
            {
                groupBoxSettings.Visible = true;
                groupBoxStops.Visible = true;
                
                Initializing = true;
                
                textName.Text = Route.Name;
                textLine.Text = Route.Line.ToString();
                textTour.Text = Route.Tour.ToString();

                textDestination.Items.Clear();
                foreach (var x in Destinations) textDestination.Items.Add(x);
                if (Route.Destination != null && textDestination.Items.Contains(Route.Destination))
                    textDestination.SelectedItem = Route.Destination;

                allStops.Items.Clear();
                foreach (var x in Stops) allStops.Items.Add(x);

                routeStops.Items.Clear();
                foreach (var x in Route.Stops) routeStops.Items.Add(x);

                Initializing = false;
            }
        }

        bool Initializing { get; set; }

        private void SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (Initializing) return;
            SettingsBusiness.SplitterDistance = splitContainer.SplitterDistance;
        }

        private void NameChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            ApplicationState.Item.Name = textName.Text;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }

        private void LineChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            if (int.TryParse(textLine.Text.Trim(), out int i))
            {
                Route.Line = i;
            }
            else
            {
                Route.Line = 1;
            }
            ApplicationState.IsUnsaved = true;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }

        private void TourChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            if (int.TryParse(textTour.Text.Trim(), out int i))
            {
                Route.Tour = i;
            }
            else
            {
                Route.Tour = 1;
            }
            ApplicationState.IsUnsaved = true;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }

        private void DestinationChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            Route.Destination = (Destination)textDestination.SelectedItem;
            ApplicationState.IsUnsaved = true;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }

        private void MoveStopUp(object sender, EventArgs e)
        {
            int index = routeStops.SelectedIndex;
            try
            {
                Route.Stops.MoveItem(index, -1);
                routeStops.Items.MoveItem(index, -1);
                ApplicationState.IsUnsaved = true;
                try
                {
                    routeStops.SelectedIndex = index - 1;
                }
                catch (Exception)
                {
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.Beep();
            }
        }

        private void MoveStopDown(object sender, EventArgs e)
        {
            int index = routeStops.SelectedIndex;
            try
            {
                Route.Stops.MoveItem(index, +1);
                routeStops.Items.MoveItem(index, +1);
                ApplicationState.IsUnsaved = true;
                try
                {
                    routeStops.SelectedIndex = index + 1;
                }
                catch (Exception)
                {
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Program.Beep();
            }
        }

        private void RemoveStop(object sender, EventArgs e)
        {
            int index = routeStops.SelectedIndex;
            if (index >= 0)
            {
                routeStops.Items.RemoveAt(index);
                Route.Stops.RemoveAt(index);
                ApplicationState.IsUnsaved = true;
                try
                {
                    routeStops.SelectedIndex = index;
                }
                catch (Exception)
                {
                    try
                    {
                        routeStops.SelectedIndex = index - 1;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            else
            {
                Program.Beep();
            }
            
        }

        private void AddStop(object sender, EventArgs e)
        {
            var selectedStop = (Stop)allStops.SelectedItem;
            if (selectedStop == null)
            {
                Program.Beep();
                return;
            }

            Route.Stops.Add(selectedStop);
            routeStops.Items.Add(selectedStop);
        }

        private void ReverseStop(object sender, EventArgs e)
        {
            Route.Stops.Reverse();
            ApplicationState.IsUnsaved = true;
            LoadItem(sender, e);
        }
    }
}
