﻿namespace Hofsuite.GUI.Controls
{
    partial class ImportTripView
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportTripView));
            this.table = new System.Windows.Forms.TableLayoutPanel();
            this.listBox = new System.Windows.Forms.CheckedListBox();
            this.label = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.selectType2 = new System.Windows.Forms.Button();
            this.selectType1 = new System.Windows.Forms.Button();
            this.selectNone = new System.Windows.Forms.Button();
            this.selectAll = new System.Windows.Forms.Button();
            this.select = new System.Windows.Forms.Label();
            this.table.SuspendLayout();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // table
            // 
            resources.ApplyResources(this.table, "table");
            this.table.Controls.Add(this.listBox, 0, 2);
            this.table.Controls.Add(this.label, 0, 0);
            this.table.Controls.Add(this.panel, 0, 1);
            this.table.Name = "table";
            // 
            // listBox
            // 
            this.listBox.CheckOnClick = true;
            resources.ApplyResources(this.listBox, "listBox");
            this.listBox.FormattingEnabled = true;
            this.listBox.Name = "listBox";
            // 
            // label
            // 
            resources.ApplyResources(this.label, "label");
            this.label.Name = "label";
            // 
            // panel
            // 
            this.panel.Controls.Add(this.selectType2);
            this.panel.Controls.Add(this.selectType1);
            this.panel.Controls.Add(this.selectNone);
            this.panel.Controls.Add(this.selectAll);
            this.panel.Controls.Add(this.select);
            resources.ApplyResources(this.panel, "panel");
            this.panel.Name = "panel";
            // 
            // selectType2
            // 
            resources.ApplyResources(this.selectType2, "selectType2");
            this.selectType2.Name = "selectType2";
            this.selectType2.UseVisualStyleBackColor = true;
            this.selectType2.Click += new System.EventHandler(this.SelectType2);
            // 
            // selectType1
            // 
            resources.ApplyResources(this.selectType1, "selectType1");
            this.selectType1.Name = "selectType1";
            this.selectType1.UseVisualStyleBackColor = true;
            this.selectType1.Click += new System.EventHandler(this.SelectType1);
            // 
            // selectNone
            // 
            resources.ApplyResources(this.selectNone, "selectNone");
            this.selectNone.Name = "selectNone";
            this.selectNone.UseVisualStyleBackColor = true;
            this.selectNone.Click += new System.EventHandler(this.SelectNone);
            // 
            // selectAll
            // 
            resources.ApplyResources(this.selectAll, "selectAll");
            this.selectAll.Name = "selectAll";
            this.selectAll.UseVisualStyleBackColor = true;
            this.selectAll.Click += new System.EventHandler(this.SelectAll);
            // 
            // select
            // 
            resources.ApplyResources(this.select, "select");
            this.select.Name = "select";
            // 
            // ImportTripView
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.table);
            this.Name = "ImportTripView";
            this.table.ResumeLayout(false);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table;
        private System.Windows.Forms.CheckedListBox listBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button selectType2;
        private System.Windows.Forms.Button selectType1;
        private System.Windows.Forms.Button selectNone;
        private System.Windows.Forms.Button selectAll;
        private System.Windows.Forms.Label select;
    }
}
