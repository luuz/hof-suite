﻿using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    partial class ImportTripView : UserControl
    {
        public ImportTripView(Map map)
        {
            InitializeComponent();
            CurrentMap = map;
            foreach (var item in MapLoader.GetAllTripFiles(map)) listBox.Items.Add(item);
        }

        public Map CurrentMap { get; }

        public List<TripFile> GetSelectedTrips()
            => listBox.CheckedItems.OfType<TripFile>().ToList();

        private void SelectAll(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
            {
                listBox.SetItemChecked(i, true);
            }
        }

        private void SelectNone(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
            {
                listBox.SetItemChecked(i, false);
            }
        }

        private void SelectType1(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
            {
                if (!((TripFile)listBox.Items[i]).IsStationLink)
                {
                    listBox.SetItemChecked(i, true);
                }
            }
        }

        private void SelectType2(object sender, EventArgs e)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
            {
                if (((TripFile)listBox.Items[i]).IsStationLink)
                {
                    listBox.SetItemChecked(i, true);
                }
            }
        }
    }
}
