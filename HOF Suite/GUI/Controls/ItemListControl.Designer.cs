﻿namespace Hofsuite.GUI.Controls
{
    partial class ItemListControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemListControl));
            this.tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.listBox = new System.Windows.Forms.ListBox();
            this.itemLabel = new System.Windows.Forms.Label();
            this.nextItem = new System.Windows.Forms.Button();
            this.previousItem = new System.Windows.Forms.Button();
            this.itemButtonBar = new System.Windows.Forms.TableLayoutPanel();
            this.addKpp = new System.Windows.Forms.Button();
            this.addAddplate = new System.Windows.Forms.Button();
            this.removeItem = new System.Windows.Forms.Button();
            this.duplicateItem = new System.Windows.Forms.Button();
            this.addItem = new System.Windows.Forms.Button();
            this.moveItemUp = new System.Windows.Forms.Button();
            this.moveItemDown = new System.Windows.Forms.Button();
            this.sortItemsByCode = new System.Windows.Forms.Button();
            this.sortItemsByName = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayout.SuspendLayout();
            this.itemButtonBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayout
            // 
            resources.ApplyResources(this.tableLayout, "tableLayout");
            this.tableLayout.Controls.Add(this.listBox, 0, 0);
            this.tableLayout.Controls.Add(this.itemLabel, 1, 1);
            this.tableLayout.Controls.Add(this.nextItem, 2, 1);
            this.tableLayout.Controls.Add(this.previousItem, 0, 1);
            this.tableLayout.Controls.Add(this.itemButtonBar, 3, 0);
            this.tableLayout.Name = "tableLayout";
            // 
            // listBox
            // 
            this.tableLayout.SetColumnSpan(this.listBox, 3);
            resources.ApplyResources(this.listBox, "listBox");
            this.listBox.FormattingEnabled = true;
            this.listBox.Name = "listBox";
            this.listBox.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // itemLabel
            // 
            resources.ApplyResources(this.itemLabel, "itemLabel");
            this.itemLabel.Name = "itemLabel";
            // 
            // nextItem
            // 
            resources.ApplyResources(this.nextItem, "nextItem");
            this.nextItem.FlatAppearance.BorderSize = 0;
            this.nextItem.Name = "nextItem";
            this.toolTip.SetToolTip(this.nextItem, resources.GetString("nextItem.ToolTip"));
            this.nextItem.UseVisualStyleBackColor = true;
            this.nextItem.Click += new System.EventHandler(this.NextItem);
            // 
            // previousItem
            // 
            resources.ApplyResources(this.previousItem, "previousItem");
            this.previousItem.FlatAppearance.BorderSize = 0;
            this.previousItem.Name = "previousItem";
            this.toolTip.SetToolTip(this.previousItem, resources.GetString("previousItem.ToolTip"));
            this.previousItem.UseVisualStyleBackColor = true;
            this.previousItem.Click += new System.EventHandler(this.PreviousItem);
            // 
            // itemButtonBar
            // 
            resources.ApplyResources(this.itemButtonBar, "itemButtonBar");
            this.itemButtonBar.Controls.Add(this.addKpp, 0, 4);
            this.itemButtonBar.Controls.Add(this.addAddplate, 0, 3);
            this.itemButtonBar.Controls.Add(this.removeItem, 0, 2);
            this.itemButtonBar.Controls.Add(this.duplicateItem, 0, 1);
            this.itemButtonBar.Controls.Add(this.addItem, 0, 0);
            this.itemButtonBar.Controls.Add(this.moveItemUp, 0, 6);
            this.itemButtonBar.Controls.Add(this.moveItemDown, 0, 7);
            this.itemButtonBar.Controls.Add(this.sortItemsByCode, 0, 9);
            this.itemButtonBar.Controls.Add(this.sortItemsByName, 0, 10);
            this.itemButtonBar.Name = "itemButtonBar";
            // 
            // addKpp
            // 
            resources.ApplyResources(this.addKpp, "addKpp");
            this.addKpp.FlatAppearance.BorderSize = 0;
            this.addKpp.Image = global::Hofsuite.Properties.Resources.Special;
            this.addKpp.Name = "addKpp";
            this.toolTip.SetToolTip(this.addKpp, resources.GetString("addKpp.ToolTip"));
            this.addKpp.UseVisualStyleBackColor = true;
            this.addKpp.Click += new System.EventHandler(this.AddKppDestinations);
            // 
            // addAddplate
            // 
            resources.ApplyResources(this.addAddplate, "addAddplate");
            this.addAddplate.FlatAppearance.BorderSize = 0;
            this.addAddplate.Image = global::Hofsuite.Properties.Resources.Special;
            this.addAddplate.Name = "addAddplate";
            this.toolTip.SetToolTip(this.addAddplate, resources.GetString("addAddplate.ToolTip"));
            this.addAddplate.UseVisualStyleBackColor = true;
            this.addAddplate.Click += new System.EventHandler(this.AddAddplateDestination);
            // 
            // removeItem
            // 
            resources.ApplyResources(this.removeItem, "removeItem");
            this.removeItem.FlatAppearance.BorderSize = 0;
            this.removeItem.Image = global::Hofsuite.Properties.Resources.Minus;
            this.removeItem.Name = "removeItem";
            this.toolTip.SetToolTip(this.removeItem, resources.GetString("removeItem.ToolTip"));
            this.removeItem.UseVisualStyleBackColor = true;
            this.removeItem.Click += new System.EventHandler(this.RemoveItem);
            // 
            // duplicateItem
            // 
            resources.ApplyResources(this.duplicateItem, "duplicateItem");
            this.duplicateItem.FlatAppearance.BorderSize = 0;
            this.duplicateItem.Image = global::Hofsuite.Properties.Resources.Copy;
            this.duplicateItem.Name = "duplicateItem";
            this.toolTip.SetToolTip(this.duplicateItem, resources.GetString("duplicateItem.ToolTip"));
            this.duplicateItem.UseVisualStyleBackColor = true;
            this.duplicateItem.Click += new System.EventHandler(this.DuplicateItem);
            // 
            // addItem
            // 
            resources.ApplyResources(this.addItem, "addItem");
            this.addItem.FlatAppearance.BorderSize = 0;
            this.addItem.Image = global::Hofsuite.Properties.Resources.Plus;
            this.addItem.Name = "addItem";
            this.toolTip.SetToolTip(this.addItem, resources.GetString("addItem.ToolTip"));
            this.addItem.UseVisualStyleBackColor = true;
            this.addItem.Click += new System.EventHandler(this.AddItem);
            // 
            // moveItemUp
            // 
            resources.ApplyResources(this.moveItemUp, "moveItemUp");
            this.moveItemUp.FlatAppearance.BorderSize = 0;
            this.moveItemUp.Image = global::Hofsuite.Properties.Resources.Up;
            this.moveItemUp.Name = "moveItemUp";
            this.toolTip.SetToolTip(this.moveItemUp, resources.GetString("moveItemUp.ToolTip"));
            this.moveItemUp.UseVisualStyleBackColor = true;
            this.moveItemUp.Click += new System.EventHandler(this.MoveItemUp);
            // 
            // moveItemDown
            // 
            resources.ApplyResources(this.moveItemDown, "moveItemDown");
            this.moveItemDown.FlatAppearance.BorderSize = 0;
            this.moveItemDown.Image = global::Hofsuite.Properties.Resources.Down;
            this.moveItemDown.Name = "moveItemDown";
            this.toolTip.SetToolTip(this.moveItemDown, resources.GetString("moveItemDown.ToolTip"));
            this.moveItemDown.UseVisualStyleBackColor = true;
            this.moveItemDown.Click += new System.EventHandler(this.MoveItemDown);
            // 
            // sortItemsByCode
            // 
            resources.ApplyResources(this.sortItemsByCode, "sortItemsByCode");
            this.sortItemsByCode.FlatAppearance.BorderSize = 0;
            this.sortItemsByCode.Image = global::Hofsuite.Properties.Resources.Sort_09;
            this.sortItemsByCode.Name = "sortItemsByCode";
            this.toolTip.SetToolTip(this.sortItemsByCode, resources.GetString("sortItemsByCode.ToolTip"));
            this.sortItemsByCode.UseVisualStyleBackColor = true;
            this.sortItemsByCode.Click += new System.EventHandler(this.SortByCode);
            // 
            // sortItemsByName
            // 
            resources.ApplyResources(this.sortItemsByName, "sortItemsByName");
            this.sortItemsByName.FlatAppearance.BorderSize = 0;
            this.sortItemsByName.Image = global::Hofsuite.Properties.Resources.Sort_AZ;
            this.sortItemsByName.Name = "sortItemsByName";
            this.toolTip.SetToolTip(this.sortItemsByName, resources.GetString("sortItemsByName.ToolTip"));
            this.sortItemsByName.UseVisualStyleBackColor = true;
            this.sortItemsByName.Click += new System.EventHandler(this.SortByName);
            // 
            // ItemListControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayout);
            this.Name = "ItemListControl";
            this.tableLayout.ResumeLayout(false);
            this.tableLayout.PerformLayout();
            this.itemButtonBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayout;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.Label itemLabel;
        private System.Windows.Forms.Button nextItem;
        private System.Windows.Forms.Button previousItem;
        private System.Windows.Forms.TableLayoutPanel itemButtonBar;
        private System.Windows.Forms.Button removeItem;
        private System.Windows.Forms.Button duplicateItem;
        private System.Windows.Forms.Button addItem;
        private System.Windows.Forms.Button moveItemUp;
        private System.Windows.Forms.Button moveItemDown;
        private System.Windows.Forms.Button sortItemsByCode;
        private System.Windows.Forms.Button sortItemsByName;
        private System.Windows.Forms.Button addKpp;
        private System.Windows.Forms.Button addAddplate;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
