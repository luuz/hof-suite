﻿using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;
using Hofsuite.GUI.Forms;
using Hofsuite.Properties;

using System;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    partial class ImportMapView : UserControl
    {
        public ImportMapView(ImportForm form)
        {
            InitializeComponent();
            this.form = form;

            foreach (Map map in MapLoader.GetAllMaps())
            {
                if (map.Image != null)
                {
                    imageList.Images.Add(map.Displayname, map.Image);
                    map.ImageKey = map.Displayname;
                }
                else if (!imageList.Images.ContainsKey("DEFAULT"))
                {
                    imageList.Images.Add("DEFAULT", Resources.NoMapImage);
                    map.ImageKey = "DEFAULT";
                }
                else
                {
                    map.ImageKey = "DEFAULT";
                }
                listView.Items.Add(map);
            }
        }

        ImportForm form;

        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedMap = listView.SelectedItems[0] as Map;
                form.InputIsCorrect = true;
            }
            catch (Exception)
            {
                SelectedMap = null;
                form.InputIsCorrect = false;
            }
        }

        public Map SelectedMap { get; private set; }
    }
}
