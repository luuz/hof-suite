﻿using Hofsuite.Core;
using Hofsuite.Core.Business;

using System;
using System.Windows.Forms;

namespace Hofsuite.GUI.Controls
{
    public partial class StopView : UserControl
    {
        public StopView()
        {
            Initializing = true;

            InitializeComponent();
            splitContainer.SplitterDistance = SettingsBusiness.SplitterDistance;

            Control newControl = new ItemListControl(ItemType.Stop)
            {
                Dock = DockStyle.Fill
            };
            splitContainer.Panel1.Controls.Add(newControl);

            newControl = new StringListView(StringType.Stop)
            {
                Dock = DockStyle.Fill
            };
            groupBoxStrings.Controls.Add(newControl);
            Initializing = false;
            
            LoadItem(this, EventArgs.Empty);
            
            ApplicationState.ItemChanged += LoadItem;
            ApplicationState.TabChanged += TabChanged;
        }

        private void TabChanged(object sender, EventArgs e)
        {
            ApplicationState.ItemChanged -= LoadItem;
            ApplicationState.TabChanged -= TabChanged;
        }

        private void LoadItem(object sender, EventArgs e)
        {
            if (ApplicationState.Item == null)
            {
                groupBoxSettings.Visible = false;
                groupBoxStrings.Visible = false;
            }
            else
            {
                groupBoxSettings.Visible = true;
                groupBoxStrings.Visible = true;

                Initializing = true;
                textName.Text = ApplicationState.Item.Name;
                Initializing = false;
            }
        }

        bool Initializing { get; set; }

        private void SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (Initializing) return;
            SettingsBusiness.SplitterDistance = splitContainer.SplitterDistance;
        }

        private void CheckMaskChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            SettingsBusiness.MaskEnabled = checkMask.Checked;
        }

        private void NameChanged(object sender, EventArgs e)
        {
            if (Initializing) return;
            ApplicationState.Item.Name = textName.Text;
            ApplicationState.IsUnsaved = true;
            ApplicationState.InvokeItemPropertiesChangedEvent();
        }
    }
}
