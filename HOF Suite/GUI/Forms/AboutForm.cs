﻿using Hofsuite.Core.Business;

using System;
using System.IO;
using System.Windows.Forms;

namespace Hofsuite.GUI.Forms
{
    partial class AboutForm : Form
	{
		public AboutForm()
		{
			InitializeComponent();
			versionLabel.Text = $"Version {Program.GetApplicationVersion()}";
			licenseText.Lines = (File.Exists(Properties.Text.FILE_License)) ?
				File.ReadAllLines(Properties.Text.FILE_License) : new string[] { Properties.Text.FILE_License };
		}

		private void Button_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void GitlabButton_Click(object sender, EventArgs e)
		{
            Program.OpenLink(SettingsBusiness.GetLink(Core.HyperlinkType.Gitlab));
		}

		private void SupportButton_Click(object sender, EventArgs e)
        {
            Program.OpenLink(SettingsBusiness.GetLink(Core.HyperlinkType.Forum));
        }

		private void DownloadButton_Click(object sender, EventArgs e)
        {
            Program.OpenLink(SettingsBusiness.GetLink(Core.HyperlinkType.Webdisk));
        }
	}
}
