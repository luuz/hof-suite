﻿using Hofsuite.Core.Business;
using Hofsuite.Core.Structs;
using Hofsuite.GUI.Controls;

using System;
using System.Windows.Forms;

namespace Hofsuite.GUI.Forms
{
    partial class ImportForm : Form
	{
		public ImportForm()
		{
			InitializeComponent();
            CurrentControl = 0;
		}

		public Hof NewHof { get; private set; }

        bool _inputIsCorrect;
        public bool InputIsCorrect
        {
            get => _inputIsCorrect;
            set
            {
                continueButton.Enabled = value;
                _inputIsCorrect = value;
            }
        }

        ImportMapView mapView;
        ImportTripView tripView;

        const int VIEW_COUNT = 2;
        int _currentControl;
        public int CurrentControl
        {
            get => _currentControl;
            set
            {
                backButton.Enabled = value != 0;
                continueButton.Text = value == VIEW_COUNT - 1 ? Properties.Text.MSG_Finish : Properties.Text.MSG_Continue;
                LoadControl(value);

                _currentControl = value;
            }
        }

        void LoadControl(int i)
        {
            if (panel.Controls.Count > 0) panel.Controls.RemoveAt(0);

            Control newControl;
            switch (i)
            {
                case 0:
                    if (mapView == null) mapView = new ImportMapView(this);
                    newControl = mapView;
                    InputIsCorrect = false;
                    break;

                case 1:
                    if (tripView == null || mapView.SelectedMap != tripView.CurrentMap)
                        tripView = new ImportTripView(mapView.SelectedMap);
                    newControl = tripView;
                    InputIsCorrect = true;
                    break;

                default:
                    throw new ArgumentException("Index could only be 0 or 1!");
            }

            newControl.Dock = DockStyle.Fill;
            panel.Controls.Add(newControl);
        }

        private void Continue(object sender, EventArgs e)
        {
            if (CurrentControl < VIEW_COUNT - 1) CurrentControl++;
            else
            {
                // Dialog is finished
                NewHof = MapLoader.GetHoffileFromTrips(tripView.CurrentMap, tripView.GetSelectedTrips());
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void GoBack(object sender, EventArgs e)
        {
            CurrentControl--;
        }

        private void Cancel(object sender, EventArgs e)
        {
            Close();
        }
    }
}
