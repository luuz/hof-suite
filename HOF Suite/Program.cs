﻿using Hofsuite.Core.Business;
using Hofsuite.GUI.Forms;
using Hofsuite.Properties;

using System;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Windows.Forms;

namespace Hofsuite
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Notify and cancel starting the application if there are missing files
            var missingFiles = PathBusiness.GetMissingFiles();
            if (missingFiles.Count > 0)
            {
                string missing = "\r\n";
                foreach (var s in missingFiles.Select(s => $"- {s}\r\n")) missing += s;
                MessageBox.Show(string.Format(Text.MSG_MissingFiles, missing), Text.TITLE_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // If Omsipath setting is not valid, ask the user for it
            if (PathBusiness.ValidOmsipath == "")
                PathBusiness.ValidOmsipath = AskForOmsipath();

            var file = Environment.GetCommandLineArgs().Length > 1 ? Environment.GetCommandLineArgs()[1] : null;
            if (PathBusiness.CheckIfFileExists(file))
            {
                Application.Run(new MainForm(file));
            }
            else
            {
                Application.Run(new MainForm());
            }
        }

        public static string GetApplicationName(bool appendVersion)
        {
            if (appendVersion) return $"{Assembly.GetExecutingAssembly().GetName().Name} {GetApplicationVersion()}";
            else return Assembly.GetExecutingAssembly().GetName().Name;
        }

        public static string GetApplicationVersion()
        {
            string debug = "";
#if DEBUG
            debug = " Debug";
#endif
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            if (version.Build == 0) return $"{version.Major}.{version.Minor}{debug}";
            else return $"{version.Major}.{version.Minor}.{version.Build}{debug}";
        }

        public static string GetApplicationCopyright()
        {
            return ((AssemblyCopyrightAttribute)
                Assembly.GetExecutingAssembly().GetCustomAttribute(typeof(AssemblyCopyrightAttribute))).Copyright;
        }

        public static void OpenTextFile(string filename)
        {
            try
            {
                OpenLink("notepad++.exe", filename);
            }
            catch (Exception)
            {
                OpenLink("notepad.exe", filename);
            }
        }

        public static void OpenLink(string filename, string args = null)
        {
            try
            {
                Process.Start(filename, args);
            }
            catch (Exception)
            {
                Beep();
            }
            
        }

        public static void Beep() => SystemSounds.Beep.Play();

        /// <summary>
        /// Ask the user for Omsipath and return null if the user ignores the warning.
        /// </summary>
        static string AskForOmsipath()
        {
            // Break if the user has disabled the warning
            if (!SettingsBusiness.EnableOmsipathWarning) return null;

            var dialog = new FolderBrowserDialog
            {
                Description = Text.MSG_Omsipath_Select,
                RootFolder = Environment.SpecialFolder.Desktop,
                ShowNewFolderButton = false
            };

            // Return the correct path if the user has selected the correct path
            if (dialog.ShowDialog() == DialogResult.OK && PathBusiness.OmsipathIsValid(dialog.SelectedPath))
            {
                return dialog.SelectedPath;
            }
            else
            {
                // Warn the user and ask to continue
                if (MessageBox.Show(string.Format(Text.MSG_Omsipath_NotValid, dialog.SelectedPath) + " " + Text.ASK_Continue, Text.TITLE_Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    // Ask the user to never ask again
                    if (MessageBox.Show(Text.ASK_NeverAgain, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SettingsBusiness.EnableOmsipathWarning = false;
                    }
                    // Return null because the user wants to ignore warning
                    return null;
                }
                // User not continues: Ask again!
                else return AskForOmsipath();
            }
        }
    }
}
