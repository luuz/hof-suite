﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Hofsuite.Core.Business
{
    static class PathBusiness
    {
        #region Private members

        /// <summary>
        /// Possible paths for OMSI installation path
        /// </summary>
        static readonly HashSet<string> possiblePaths = new HashSet<string>
        {
            @"C:\Program Files\Steam\SteamApps\common\OMSI 2",
            @"C:\Program Files (x86)\Steam\SteamApps\common\OMSI 2",
            @"D:\Program Files\Steam\SteamApps\common\OMSI 2",
            @"D:\Program Files (x86)\Steam\SteamApps\common\OMSI 2",
        };

        /// <summary>
        /// Required files for HOF Suite to work
        /// </summary>
        static readonly HashSet<string> requiredFiles = new HashSet<string>
        {
            @"de-DE\HOF Suite.resources.dll",
            //@"FreeImage.dll",
            //@"FreeImageNET.dll",
            @"HOF Suite.exe.config",
        };

        #endregion

        #region Omsipath operations

        /// <summary>
        /// Valid Omsipath or empty string
        /// </summary>
        public static string ValidOmsipath
        {
            get => OmsipathIsValid(SettingsBusiness.Omsipath) ? SettingsBusiness.Omsipath : "";
            set => SettingsBusiness.Omsipath = OmsipathIsValid(value) ? value : null;
        }

        /// <summary>
        /// Try detect Omsipath automatically. Returns null if not successful.
        /// </summary>
        public static string DetectOmsipath()
        {
            foreach (var s in possiblePaths) if (OmsipathIsValid(s)) return s;
            return null;
        }

        /// <summary>
        /// Check if Omsipath is valid
        /// </summary>
        public static bool OmsipathIsValid(string path) =>
            Directory.Exists(path) &&
            File.Exists(path + @"\Omsi.exe") &&
            Directory.Exists(path + @"\Vehicles") &&
            Directory.Exists(path + @"\maps");

        #endregion

        #region Directory getter

        /// <summary>
        /// Path for vehicles
        /// </summary>
        public static string GetVehicleDirectory(string subfolderName = null) =>
            subfolderName.IsEmpty() ?
            ValidOmsipath + @"\Vehicles" :
            ValidOmsipath + @"\Vehicles\" + subfolderName;

        /// <summary>
        /// Path for vehicles
        /// </summary>
        public static string[] GetAllVehicleDirectories() =>
            Directory.GetDirectories(GetVehicleDirectory());

        /// <summary>
        /// Path for maps
        /// </summary>
        public static string GetMapDirectory(string subfolderName = null) =>
            subfolderName.IsEmpty() ?
            ValidOmsipath + @"\maps" :
            ValidOmsipath + @"\maps\" + subfolderName;

        /// <summary>
        /// Default path for announcements
        /// </summary>
        public static string GetAnnouncementDirectory(string subfolderName = null) =>
            subfolderName.IsEmpty() ?
            ValidOmsipath + @"\Vehicles\Announcements" :
            ValidOmsipath + @"\Vehicles\Announcements\" + subfolderName;

        /// <summary>
        /// Default path for rollsigns
        /// </summary>
        public static string GetRollsignDirectory(string subfolderName = null) =>
            subfolderName.IsEmpty() ?
            ValidOmsipath + @"\Vehicles\Anzeigen\Rollband_SD79" :
            ValidOmsipath + @"\Vehicles\Anzeigen\Rollband_SD79\" + subfolderName;

        /// <summary>
        /// Default path for sideplates
        /// </summary>
        public static string GetSideplateDirectory(string subfolderName = null) =>
            subfolderName.IsEmpty() ?
            ValidOmsipath + @"\Vehicles\Anzeigen\Seitenschilder" :
            ValidOmsipath + @"\Vehicles\Anzeigen\Seitenschilder\" + subfolderName;

        /// <summary>
        /// HOF-specific texture path (rollsigns etc.)
        /// </summary>
        public static string GetImagesDirectory(ImageType type)
        {
            switch (type)
            {
                case ImageType.Rollsign:
                    return GetRollsignDirectory(ApplicationState.Hof.Settings.GetGlobalString(GlobalStringType.RollsignFolder));
                case ImageType.Addplate:
                    return ValidOmsipath + @"\Vehicles\Anzeigen\SteckSchilder";
                case ImageType.Krueger:
                    return ValidOmsipath + @"\Vehicles\Anzeigen\Krueger";
                default:
                    throw new System.ArgumentException("Enum not valid", "type");
            }
        }

        #endregion

        /// <summary>
        /// Check if HOF Suite could not find needed files
        /// </summary>
        public static HashSet<string> GetMissingFiles() =>
            requiredFiles.Where(file => !File.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + file)).ToHashSet();
        
        public static bool CheckIfDirectoryExists(string directory) =>
            Directory.Exists(directory);

        public static bool CheckIfFileExists(string file) =>
            File.Exists(file);

        public static void CreateDirectory(string directory) =>
            Directory.CreateDirectory(directory);

        public static string GetFilenameFromPath(string path) =>
            Path.GetFileName(path);

        public static string ShortenFilename(string filename)
        {
            if (filename.StartsWith(SettingsBusiness.Omsipath)) {
                return filename.Replace(SettingsBusiness.Omsipath + "\\", "");
            }
            else
            {
                return filename;
            }
        }

        public static string GetTemporaryFilename() =>
            Path.GetTempFileName();

        public static void DeleteFile(string filename) =>
            File.Delete(filename);

        public static void CopyFile(string source, string target) =>
            File.Copy(source, target, true);

        public static string GetDirectoryName(string fullDirectory) =>
            fullDirectory.Substring(fullDirectory.LastIndexOf('\\') + 1);
    }
}
