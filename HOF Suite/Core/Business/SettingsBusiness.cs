﻿using Hofsuite.Properties;

using System;
using System.Drawing;

namespace Hofsuite.Core.Business
{
    static class SettingsBusiness
    {
        public static string GetLink(HyperlinkType type)
        {
            switch (type)
            {
                case HyperlinkType.Gitlab:
                    return Settings.Default.Gitlab;
                case HyperlinkType.Webdisk:
                    return Settings.Default.Webdisk;
                case HyperlinkType.Forum:
                    return Settings.Default.Forum;
                default:
                    throw new ArgumentException("Enum not valid", "type");
            }
        }

        /// <summary>
        /// Reset and reload the settings
        /// </summary>
        public static void ResetSettings()
        {
            Settings.Default.Reset();
            Settings.Default.Reload();
        }

        public static string Omsipath
        {
            get => Settings.Default.Omsipath;
            set
            {
                Settings.Default.Omsipath = value;
                Settings.Default.Save();
                Settings.Default.Reload();
            }
        }

        public static string Stringtemplate
        {
            get => Settings.Default.Stringtemplate;
            set
            {
                Settings.Default.Stringtemplate = value;
                Settings.Default.Save();
                Settings.Default.Reload();
            }
        }

        public static int SplitterDistance
        {
            get => Settings.Default.SplitterDistance;
            set
            {
                Settings.Default.SplitterDistance = value;
                Settings.Default.Save();
                Settings.Default.Reload();
            }
        }

        public static bool MaskEnabled
        {
            get => Settings.Default.MaskEnabled;
            set
            {
                Settings.Default.MaskEnabled = value;
                Settings.Default.Save();
                Settings.Default.Reload();
                ApplicationState.InvokeMaskChangedEvent();
            }
        }

        public static bool EnableOmsipathWarning
        {
            get => Settings.Default.EnableOmsipathWarning;
            set
            {
                Settings.Default.EnableOmsipathWarning = value;
                Settings.Default.Save();
                Settings.Default.Reload();
            }
        }
    }
}
