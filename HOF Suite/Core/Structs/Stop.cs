﻿using System;

namespace Hofsuite.Core.Structs
{
    class Stop : Item, IComparable, IHasStringList
    {
        public Stop(string name = "", StringList stringList = null)
        {
            Name = name;
            StringList = stringList ?? new StringList();
        }

        public StringList StringList { get; }

        public override string ToString() => Name;

        public override void Write(Writer writer)
        {
            writer.Write(Name);
            StringList.Write(writer);
            writer.WriteLine();
        }

        public void Autocomplete()
        {
            // IBIS 1
            if (Name.Length <= 16) StringList.SetString(0, Name.ToUpper());
            else
            {
                Name = ReplaceLongNames(Name);
                if (Name.Length <= 16) StringList.SetString(0, Name.ToUpper());
            }

            // Inner display
            if (Name.Length <= 20) StringList.SetString(1, Name);
            else
            {
                Name = ReplaceLongNames(Name);
                if (Name.Length <= 20) StringList.SetString(1, Name);
                else
                {
                    string[] words = Name.Split(autocompleteSeparators);
                    if (words.Length == 2)
                    {
                        StringList.SetString(1, words[0]);
                        StringList.SetString(2, words[1]);
                    }
                }
            }

            // IBIS 2
            if (Name.Length <= 20) StringList.SetString(3, Name);
            else
            {
                Name = ReplaceLongNames(Name);
                if (Name.Length <= 20) StringList.SetString(3, Name);
            }
        }

        static readonly char[] autocompleteSeparators = { ' ', '-', '/', '_', ',' };

        static string ReplaceLongNames(string name)
        {
            return name
                .Replace("Hauptbahnhof", "Hbf")
                .Replace("hauptbahnhof", "hbf")
                .Replace("Bahnhof", "Bf")
                .Replace("bahnhof", "bf")
                .Replace("Straße", "Str")
                .Replace("straße", "str")
                .Replace("Strasse", "Str")
                .Replace("strasse", "str")
                .Replace("Platz", "Pl")
                .Replace("Weg", " W")
                .Replace("weg", "w")
                .Replace("Siedlung", "Siedl")
                .Replace("siedlung", "siedl")
                .Replace("International", "Int.")
                .Replace("Road", "Rd.")
                .Replace("Street", "St.")
                .Replace("Boulevard", "Blvd.");
        }

        public override Item DuplicateItem() => new Stop(Name, new StringList(StringList));

        public int CompareTo(object other) => Name.CompareTo(((Stop)other).Name);
    }
}
