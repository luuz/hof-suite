﻿using Hofsuite.Properties;

using System;
using System.Collections.Generic;

namespace Hofsuite.Core.Structs
{
    class TripFile : IComparable
    {
        public TripFile(string filename)
        {
            Filename = filename;
            Stops = new List<string>();
        }

        public TripFile(string filename, string destination, string line, List<string> stops, bool isStationLink)
        {
            Filename = filename;
            Destination = destination;
            Line = line;
            Stops = stops ?? new List<string>();
            IsStationLink = isStationLink;
        }

        public string Filename { get; set; }

        public string Destination { get; set; }

        public string Line { get; set; }

        public List<string> Stops { get; set; }

        public bool IsStationLink { get; set; }

        public int CompareTo(object obj)
        {
            return ToString().CompareTo(obj.ToString());
        }

        public override string ToString()
        {
            string s = $"{Text.Type} {(IsStationLink ? "2" : "1")} - {Filename}";
            if (Stops.Count == 1) s += $" ({Stops[0]})";
            else if (Stops.Count > 1) s += $" ({Stops[0]} > {Stops[Stops.Count - 1]})";
            return s;
        }
    }
}
