﻿using System.IO;
using System.Text;

namespace Hofsuite.Core.Structs
{
    class Writer : StreamWriter
    {
        public Writer(string path) : base(path, false, Encoding.Default)
        {
        }

        static readonly char[] trimChars = { '\t', ' ' };

        public int CurrentLine { private set; get; } = 0;

        public void WriteTitle(string title)
        {
            int length = 2 * title.Length + 3;
            for (int i = 0; i < length; i++) Write("*");
            WriteLine();

            Write("* ");
            foreach (var c in title.ToCharArray()) Write(c + " ");
            WriteLine('*');

            for (int i = 0; i < length; i++) Write("*");
            WriteLine();
            WriteLine();
        }

        public override void WriteLine()
        {
            CurrentLine++;
            base.WriteLine();
        }

        public override void WriteLine(string value)
        {
            CurrentLine++;
            base.WriteLine(value?.TrimEnd(trimChars));
        }

        public override void WriteLine(int value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(char value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(char[] buffer)
        {
            CurrentLine++;
            base.WriteLine(buffer);
        }

        public override void WriteLine(char[] buffer, int index, int count)
        {
            CurrentLine++;
            base.WriteLine(buffer, index, count);
        }

        public override void WriteLine(bool value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(uint value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(long value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(ulong value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(float value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(double value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(decimal value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(object value)
        {
            CurrentLine++;
            base.WriteLine(value);
        }

        public override void WriteLine(string format, object arg0)
        {
            CurrentLine++;
            base.WriteLine(format, arg0);
        }

        public override void WriteLine(string format, object arg0, object arg1)
        {
            CurrentLine++;
            base.WriteLine(format, arg0, arg1);
        }

        public override void WriteLine(string format, object arg0, object arg1, object arg2)
        {
            CurrentLine++;
            base.WriteLine(format, arg0, arg1, arg2);
        }

        public override void WriteLine(string format, params object[] arg)
        {
            CurrentLine++;
            base.WriteLine(format, arg);
        }
    }
}
