﻿using System;

namespace Hofsuite.Core.Structs
{
    class Destination : Item, IComparable, IExtraComparable, IHasStringList
    {
        public Destination(string name = "", int code = 0, bool allexit = false, StringList stringList = null)
        {
            Name = name;
            Code = code;
            Allexit = allexit;
            StringList = stringList ?? new StringList();
        }

        public int Code { get; set; }

        public bool Allexit { get; set; }

        public StringList StringList { get; }

        public override Item DuplicateItem() => new Destination(Name, Code, Allexit, new StringList(StringList));

        public override string ToString() => $"{Code}: {Name}";

        public override void Write(Writer writer)
        {
            if (Allexit) writer.Write(Keywords.ALLEXIT);
            writer.Write("\t");

            writer.Write(Code);
            writer.Write("\t");
            writer.Write(Name);

            StringList.Write(writer);
            writer.WriteLine();
        }

        public int CompareTo(object other) => Code.CompareTo(((Destination)other).Code);

        public int CompareByName(object other) => Name.CompareTo(((Destination)other).Name);
    }
}
