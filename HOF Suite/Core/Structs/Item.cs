﻿namespace Hofsuite.Core.Structs
{
    abstract class Item : IWritable
    {
        public abstract override string ToString();
        public abstract void Write(Writer writer);
        public abstract Item DuplicateItem();

        public string Name { get; set; }
    }
}
