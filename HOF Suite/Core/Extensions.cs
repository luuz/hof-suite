﻿using System;
using System.Collections;
using System.IO;
using System.Linq;

namespace Hofsuite.Core
{
    static class ListExtensions
    {
        public static void MoveItem(this IList list, int position, int direction)
        {
            // throw exception if the parameters are incorrect
            if (direction == 0) throw new ArgumentOutOfRangeException("The parameter 'offset' should not be 0!");
            if (position < 0 || position >= list.Count) throw new ArgumentOutOfRangeException("The parameter 'position' is out of list range!");

            // throw exception if new index is out of range
            int newIndex = position + direction;
            if (newIndex < 0 || newIndex >= list.Count) throw new ArgumentOutOfRangeException("The parameters 'position' and 'offset' leads to no valid list item!");

            var itemToMove = list[position];
            list.RemoveAt(position);
            list.Insert(newIndex, itemToMove);
        }
    }

    static class StringExtensions
    {
        public static bool IsEmpty(this string s)
        {
            return s == null || s.Trim() == "";
        }

        public static bool ContainsInvalidPathChars(this string s)
        {
            foreach (var c in Path.GetInvalidFileNameChars()) if (s.Contains(c)) return true;
            foreach (var c in Path.GetInvalidFileNameChars()) if (s.Contains(c)) return true;
            return false;
        }
    }
}
