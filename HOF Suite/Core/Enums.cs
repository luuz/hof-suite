﻿namespace Hofsuite.Core
{
    enum ShownTab {
        Settings,
        Destinations,
        Stops,
        Routes
    }

    enum HyperlinkType
    {
        Gitlab, Webdisk, Forum
    }

    enum ImageType
    {
        Rollsign = 4,
        Addplate = 6,
        Krueger = 7
    }

    enum GlobalStringType
    {
        AnnouncementFolder,
        RollsignFolder,
        SideplateFolder,
        IBISCode9,
        IBISCode8,
        IBISCode5
    }

    enum StringType
    {
        Destination,
        Stop
    }

    enum ItemType
    {
        Destination,
        Stop,
        Route
    }
}
