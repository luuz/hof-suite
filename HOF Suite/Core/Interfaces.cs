﻿using Hofsuite.Core.Structs;

using System.Collections.Generic;

namespace Hofsuite.Core
{
    interface IWritable
    {
        void Write(Writer writer);
    }

    interface IExtraComparable
    {
        int CompareByName(object other);
    }

    interface IHasStringList
    {
        StringList StringList { get; }
    }

    interface IStringtemplate
    {
        string Name_DE { get; }

        string Name_EN { get; }

        string Mask { get; }
    }

    interface IHasErrors
    {
        List<Error> Errors { get; }
    }
}
